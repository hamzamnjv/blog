<?php

namespace App\Http\Controllers;

use App\Models\Bloog;
use App\Models\Comment;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    
    public function index()
{
    $bloogs = Bloog::orderBy('created_at', 'DESC');

    if (request()->has('search')) {
        $bloogs = $bloogs->where('content', 'like', '%' . request()->get('search', '') . '%');
    }


    $bloogs = $bloogs->paginate(5);

    return view('dashboard', compact('bloogs'));
}

}

