<?php

namespace App\Http\Controllers;

use App\Models\Bloog;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store( Bloog $bloog)
    {
                
                $comment = new Comment();
                $comment->bloog_id = $bloog->id;
                $comment->user_id = auth()->id();
                $comment->content = request()->get('content');
                $comment->save();
        return redirect()->route('bloogs.show', $bloog->id)->with('success', "Comment posted successfully!");
        
    }
}
