<?php

namespace App\Http\Controllers;

use App\Models\Bloog;
use Illuminate\Http\Request;

class BloogLikeController extends Controller
{
    public function like(Bloog $bloog){
        $liker = auth()->user();

        $liker->likes()->attach($bloog);

        return redirect()->route('dashboard')->with('success',"Liked successfully!");
    }

    public function unlike(Bloog $bloog){
        $liker = auth()->user();

        $liker->likes()->detach($bloog);

        return redirect()->route('dashboard')->with('success',"Liked successfully!");
    }
}
