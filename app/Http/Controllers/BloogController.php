<?php

namespace App\Http\Controllers;


use App\Models\Bloog;
use Illuminate\Http\Request;

class bloogController extends Controller
{

    public function show(Bloog $bloog)
    {
        return view('bloogs.show', compact('bloog'));
    }
    public function store()
    {
        $validate = request()->validate([
            'content' => 'required|min:5|max:240'
            ]);

        $validate['user_id']=auth()->id();
        Bloog::create($validate);

            return redirect()->route('dashboard')->with('success', 'Bloog created Successfully');

    }

    public function destroy(Bloog $bloog)
    {
        if(auth()->id() !== $bloog->user_id){
            abort(404);
        }
        
        $bloog->delete();


        return redirect()->route('dashboard')->with('success', 'Idea deleted successfully !');
    }

    public function edit (Bloog $bloog) {

        if(auth()->id() !== $bloog->user_id){
            abort(404);
        }

        $editing = true;
        return view('Bloogs.show', compact ('bloog', 'editing'));
    }

    public function update (Bloog $bloog){
        
        if(auth()->id() !== $bloog->user_id){
            abort(404);
        }

        $validate = request()->validate([
        'content' => 'required|min:3|max: 240'
        ]);

        $bloog->update($validate);
        

        return redirect()->route('bloogs.show', $bloog->id)->with('success', "Blog updated successfuly!");  
    }


}
