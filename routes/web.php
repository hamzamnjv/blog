<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\bloogController;
use App\Http\Controllers\BloogLikeController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/bloogs/{bloog}', [bloogController::class, 'show'])->name('bloogs.show');

Route::get('/bloogs/{bloog}/edit', [bloogController::class, 'edit'])->name('bloogs.edit')->middleware('auth');

Route::put('/bloogs/{bloog}', [bloogController::class, 'update'])->name('bloogs.update')->middleware('auth');


Route::post('/bloogs', [bloogController::class, 'store'])->name('bloogs.create')->middleware('auth');

Route::delete('/bloogs/{bloog}', [bloogController::class, 'destroy'])->name('bloogs.destroy')->middleware('auth');


Route::post('/bloogs/{bloog}/comments', [CommentController::class, 'store'])->name('bloogs.comments.store')->middleware('auth');

Route::post('bloogs/{bloog}/like', [BloogLikeController::class, 'like'])->middleware('auth')->name('bloogs.like');
Route::post('bloogs/{bloog}/unlike', [BloogLikeController::class, 'unlike'])->middleware('auth')->name('bloogs.unlike');





Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/register', [AuthController::class, 'store']);

Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'authenticate']);

Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth')->name('logout');



Route::get('/terms', function () {
    return view('terms');
});
