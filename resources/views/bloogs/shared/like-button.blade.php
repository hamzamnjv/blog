<div>
    @auth
        
    
        @if (Auth::user()->likesBloog($bloog))

            <form action="{{ route('bloogs.unlike', $bloog->id) }}" method="POST">
                @csrf
                <button type="submit" class="fw-light nav-link fs-6"> <span class="fas fa-heart me-1">
                    </span> {{ $bloog->likes()->count() }} </button>
            </form>
        @else
            <form action="{{ route('bloogs.like', $bloog->id) }}" method="POST">
                @csrf
                <button type="submit" class="fw-light nav-link fs-6"> <span class="far fa-heart me-1">
                    </span> {{ $bloog->likes()->count()}} </button>
            </form>
        @endif
    @endauth


    @guest
                <a href="{{ route('login') }}" class="fw-light nav-link fs-6"> <span class="far fa-heart me-1">
            </span> {{ $bloog->likes()->count() }} </a>
    @endguest

    
</div>
