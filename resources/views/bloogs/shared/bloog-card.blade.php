<div class="card">
    <div class="px-3 pt-4 pb-2">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-flex align-items-center">


                <img style="width:50px" class="me-2 avatar-sm rounded-circle"
                    src="https://cdn.pixabay.com/photo/2022/07/18/20/06/guy-7330732_1280.png" alt="Mario Avatar">
                <div>
                    <h5 class="card-title mb-0"><a href="#"> {{$bloog->user->name}}
                        </a></h5>
                </div>
            </div>
            <div>
                <form method="POST" action="{{ route('bloogs.destroy', $bloog->id) }}">
                @csrf
                @method('delete')         
                <a class="mx-2" href="{{ route('bloogs.edit', $bloog->id) }}"> Edit </a>
                <a class="mx-2"href="{{ route('bloogs.show', $bloog->id) }}"> View </a>
                <button class="btn btn-danger btn-sm"> X </button>
                </form>

            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($editing ?? false)
                <form action="{{ route('bloogs.update',$bloog->id) }}" method="post">
                        @csrf
                        @method('put')         
                    <div class="mb-3">
                        <textarea name='content' class="form-control" id="content" rows="3"> {{$bloog->content}}</textarea>
                            @error('content')
                                <span class="fs-6 text-danger"> {{ $message }} </span>
                            @enderror
                    </div>
                    <div class="">
                        <button type="submit" class="btn btn-dark"> Update </button>
                    </div>
                </form>
        @else
        <p class="fs-6 fw-light text-muted">
            {{ $bloog->content }}
        </p>
        @endif

        <div class="d-flex justify-content-between">
        @include('bloogs.shared.like-button') {{--  like______________ --}}
            <div>
                <span class="fs-6 fw-light text-muted"> <span class="fas fa-clock"> </span>
                {{$bloog->created_at }} </span>
            </div>
        </div>
        @include('shared.comments-box')
    </div>
</div>