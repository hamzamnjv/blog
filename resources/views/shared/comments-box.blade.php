<div>
    <form action="{{ route('bloogs.comments.store', $bloog->id) }}" method="POST">
        @csrf
        <div class="mb-3">
            <textarea name="content" class="fs-6 form-control" rows="1"></textarea>
        </div>
        <div>
            <button type="submit" class="btn btn-primary btn-sm"> Post Comment </button>
        </div>
    </form>

    <hr>
    @forelse ($bloog-> comments as $comment)
        <div class="d-flex align-items-start">
        <img style="width:35px" class="me-2 avatar-sm rounded-circle"
            src="https://cdn.pixabay.com/photo/2022/07/18/20/06/guy-7330732_1280.png"
            alt="Luigi Avatar">
        <div class="w-100">
            <div class="d-flex justify-content-between">
                <h6 class="">{{ $comment->user->name }}
                </h6>
                <small class="fs-6 fw-light text-muted">  {{ $comment->created_at->diffForHumans() }} </small>
            </div>
            <p class="fs-6 mt-3 fw-light">
                {{ $comment->content }}
            </p>
        </div>
    </div>
    @empty
        
    <p class=" text-center "> No Comment found </p>
    @endforelse
    
</div>