@extends('layout.layout')


@section('content')
        <div class="row">
            <div class="col-3">
                @include('shared.left-sidebar')
            </div>
            <div class="col-6">
                @include('shared.success-message')   
                    @include('bloogs.shared.submit-bloog')
                <hr>
            {{-- staaa___________________________________________rt --}}
           
            @forelse ($bloogs as $bloog)
                <div class="mt-3">
                    @include('bloogs.shared.bloog-card')
                </div>
            @empty
                <p class="my-5 text-center "> No Results found </p>
            @endforelse  
            {{-- en___________________________________________d --}} 
                <div class="mt-3">
                    {{ $bloogs->withQueryString()->links() }}
                </div>
                
                
                
            </div>
            <div class="col-3">
                @include('shared.search-bar')
                
            </div>
        </div>
    </div>


@endsection